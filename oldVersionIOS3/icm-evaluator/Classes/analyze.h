//
//  analyze.h
//  icm-eval
//
//  Created by richard on 15/02/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CustomCell.h"
#import "ICM.h"
#import "customCellAnalyze.h"
#import "ProgressView.h"
@interface analyze :UITableViewController{
	
	UITextField * nameField;
	NSMutableArray * listIndice;
	NSMutableArray * listSection;
	NSMutableArray * listPayouts;
	NSMutableArray * listJoueurs;
	sngStruct tournoi;
	UIActivityIndicatorView *activityIndicator;
	
	int nombreJoueurs;
	UIButton *button;
	UITabBar *menuBas;
	NSTimer *timer;
	ProgressView *activityView;
	int *update;
}

- (IBAction) Edit:(id)sender;
@property(nonatomic,retain) NSMutableArray * listIndice;
@property(nonatomic,retain) NSMutableArray * listSection;
@property(nonatomic,retain) NSMutableArray * listPayouts;
@property(nonatomic,retain) NSMutableArray * listJoueurs;
@property(nonatomic,retain) UIButton *button;
@property (nonatomic, retain) IBOutlet UITabBar *menuBas;
@property  int *update;

- (IBAction)AddButtonAction:(id)sender;
- (IBAction)DeleteButtonAction:(id)sender;
-(void)startCompute;
@end
