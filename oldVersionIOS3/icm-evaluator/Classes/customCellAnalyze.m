//
//  customCellAnalyze.m
//  icm-eval
//
//  Created by richard on 05/03/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "customCellAnalyze.h"


@implementation customCellAnalyze


@synthesize Titre , equity, pourcent,poids;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
       Titre = [[UILabel alloc] initWithFrame:CGRectMake(20, 5, 120, 30)]; 
	   Titre.font = [UIFont fontWithName:@"arial" size:16.0];
       pourcent = [[UILabel alloc] initWithFrame:CGRectMake(120, 5, 120, 30)]; 
	   pourcent.font = [UIFont fontWithName:@"arial" size:18.0];
       equity = [[UILabel alloc] initWithFrame:CGRectMake(190, 5, 120, 30)]; 
	   equity.font = [UIFont fontWithName:@"arial" size:18.0];

		/*poids = [[UILabel alloc] initWithFrame:CGRectMake(75, 5, 120, 30)]; 
		poids.font = [UIFont fontWithName:@"arial" size:18.0];*/
		
		
        [self addSubview:Titre];
		[self addSubview:pourcent];
		[self addSubview:equity];
		//[self addSubview:poids];
		//Titre.text = @"1st";
		
		
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
	
    // Configure the view for the selected state
}

- (void)dealloc
{
    [Titre release];
    [super dealloc];
}

@end
