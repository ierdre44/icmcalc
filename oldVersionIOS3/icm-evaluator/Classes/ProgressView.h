//
//  ProgressView.h
//  icm-eval
//
//  Created by richard on 05/06/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ProgressView : UIViewController {
	UIActivityIndicatorView *myActivityIndicator;
}
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *myActivityIndicator;
@end
