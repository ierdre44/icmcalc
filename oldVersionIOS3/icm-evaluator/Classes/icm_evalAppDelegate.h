//
//  icm_evalAppDelegate.h
//  icm-eval
//
//  Created by richard on 09/02/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "playersListe.h"
#import "payoutsListe.h"
#import "analyze.h"
@interface icm_evalAppDelegate : NSObject <UIApplicationDelegate,UITabBarDelegate> {
    UIWindow *window;
	
	
	
    UIView  *Players;
	UIView  *Players_tableau;

	UIView  * current;
	UINavigationItem *title;
	
	UIView  *Analyze;
	UIView  *Tab_Analyze;
	//
	UITabBar *menuBas;
	playersListe *  lP;
	payoutsListe *  lPayout;
	analyze *  lAnalyze;
	int update;
	
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet  UIView *Analyze;
@property (nonatomic, retain) IBOutlet  UIView * Tab_Analyze;


@property (nonatomic, retain) IBOutlet  UIView *Players;
@property (nonatomic, retain) IBOutlet  UIView *Players_tableau;
@property (nonatomic, retain) IBOutlet UINavigationItem *title;


@property (nonatomic, retain) IBOutlet UITabBar *menuBas;

//-(IBAction)NextfromDem:(id)sender;

-(void) setPlayerTab:(id)sender;
-(void) setPayoutTab:(id)sender;
-(void) setAnalyzeTab:(id)sender;


@end

