//
//  CustomCell.h
//  TableView
//
//  Created by Marian PAUL on 29/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CustomCell : UITableViewCell {
	
    UILabel *myLabel;
	UIButton *button;
}


@property (nonatomic, retain) UILabel *myLabel;
@property (nonatomic, retain) UIButton *button;
@end
