//
//  playersListe.m
//  icm-eval
//
//  Created by richard on 09/02/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "playersListe.h"
#import "CustomCell.h"

@implementation playersListe
@synthesize listJoueurs,update;
-(void)viewWillAppear:(BOOL)animated { 

}


- (void)viewDidLoad {
	
    [super viewDidLoad];
	self.listJoueurs = [NSMutableArray arrayWithObjects: @"first player",nil];
	self.tableView.scrollEnabled = YES;
	self.tableView.userInteractionEnabled = YES;
	[super setEditing:YES animated:YES]; 
	[self.tableView setEditing:YES animated:YES];
	//self.tableView.backgroundColor = [UIColor blackColor];
	nombreJoueurs = 0;
	
	/*UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithTitle:@"Add" style:UIBarButtonItemStyleBordered target:self action:@selector(AddButtonAction:)];
	[self.navigationItem setRightBarButtonItem:addButton];
	*/
		
}


- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	
}

- (void)viewDidUnload {
	
}


- (void)dealloc {
    [super dealloc];
}


/*- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	NSLog(@"didSelectRowAtIndexPath");
	
	//[self.tableView reloadData];
	
	
}*/
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [self.listJoueurs count];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
   
    
	static NSString *CellIdentifier = @"Cell"; 
	CustomCell *cell =   (CustomCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];

	if (cell == nil) { 
		cell = [[[CustomCell alloc] 
				 initWithStyle:UITableViewCellStyleSubtitle 
				 reuseIdentifier:CellIdentifier] autorelease]; 
		
		UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
		[button addTarget:self
				   action:@selector(editPlayer:)
		 
		 forControlEvents:UIControlEventTouchDown];
		[button setBackgroundImage:[[UIImage imageNamed:@"buttonGreen.png"]
									stretchableImageWithLeftCapWidth:8.0f
									topCapHeight:0.0f]
						  forState:UIControlStateNormal];
		
		[button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
		button.titleLabel.font = [UIFont boldSystemFontOfSize:20];
		button.titleLabel.shadowColor = [UIColor lightGrayColor];
		button.titleLabel.shadowOffset = CGSizeMake(0, -1);
		//button.tag =indexPath.row ;
		[button setTitle:@"edit" forState:UIControlStateNormal];
		button.frame = CGRectMake(255.0f, 5.0f, 58.0f, 30.0f);
		[cell addSubview:button];
		cell.button = button;
		
	} 
	cell.textLabel.text = [listJoueurs 
						   objectAtIndex:indexPath.row]; 
	NSString *subtile = [NSString stringWithFormat:@"Player %d",indexPath.row+1];
	if ((indexPath.row ) < nombreJoueurs ) {
		cell.detailTextLabel.text = subtile ;
		[cell.button setTitle:@"edit" forState:UIControlStateNormal];
		cell.button.hidden = FALSE;
		cell.button.tag =indexPath.row +1;
	}
	else {
		cell.detailTextLabel.text =@"";
		cell.button.hidden = TRUE;

		
	}

	
	
	
	
	/*cell.accessoryType = 
	UITableViewCellAccessoryDisclosureIndicator; 
*/
	/*UIButton *downloadButton = [[UIButton alloc] init];
	downloadButton.titleLabel.text = @"Download";
	[downloadButton setFrame:CGRectMake(00, 0, 100, 20)];
	[cell.accessoryView addSubview:downloadButton];
	[cell.accessoryView setNeedsLayout];*/
	return cell; 
}



/*- (IBAction)DeleteButtonAction:(id)sender{
	//[dataList removeLastObject];
	NSLog(@"DELL");
	

}
*/
/*- (IBAction) Edit:(id)sender{
	NSLog(@"EDIT");
	
}*/

- (UITableViewCellEditingStyle)tableView:(UITableView *)aTableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
	if (indexPath.row == ([listJoueurs count]-1)) return UITableViewCellEditingStyleInsert;
 return UITableViewCellEditingStyleDelete;

}

/*-(IBAction)AddButtonAction:(id)sender{
	//[self.categories ;
	NSLog(@"ADD");
	

	//[categories  replaceObjectAtIndex:1 withObject:@"fsfsdfsd"];
	//[categories addObject:@"TEST"];
	//[self.tableView reloadData];
 }*/

- (void)tableView:(UITableView *)aTableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle 
forRowAtIndexPath:(NSIndexPath *)indexPath {
	if (editingStyle == UITableViewCellEditingStyleDelete) {
		NSLog(@"D %d",nombreJoueurs);
		[listJoueurs removeObjectAtIndex:indexPath.row];
		nombreJoueurs = nombreJoueurs -1;
		[self.tableView reloadData];
	} else if (editingStyle == UITableViewCellEditingStyleInsert) {
		[self showAddPlayer:indexPath.row];
		//[listJoueurs replaceObjectAtIndex:indexPath.row withObject:res];
		[listJoueurs insertObject:@"Add new player" atIndex:[listJoueurs count]];
		nombreJoueurs = nombreJoueurs +1;
		[self.tableView reloadData];
	}
}
- (IBAction) EditTable:(id)sender{
	if(self.editing)
	{
		[super setEditing:NO animated:NO];
		[self.tableView setEditing:NO animated:NO];
		[self.tableView reloadData];
		[self.navigationItem.leftBarButtonItem setTitle:@"Edit"];
		[self.navigationItem.leftBarButtonItem setStyle:UIBarButtonItemStylePlain];
	}
	else
	{
		[super setEditing:YES animated:YES];
		[self.tableView setEditing:YES animated:YES];
		[self.tableView reloadData];
		[self.navigationItem.leftBarButtonItem setTitle:@"Done"];
		[self.navigationItem.leftBarButtonItem setStyle:UIBarButtonItemStyleDone];
	}
}
/** EDIT PLAYER **/
-(void) editPlayer:(id) sender{
	*update = 1;
	UIButton *button = (UIButton *)sender;
	NSLog(@"-->%d ", button.tag-1);
	UIAlertView* dialog = [[UIAlertView alloc] init];
	dialog.transform = CGAffineTransformMakeTranslation(0,60);

	
	[dialog setDelegate:self];
	[dialog setTitle:@"Enter new chip count"];
	[dialog setMessage:@" "];
	//[dialog addButtonWithTitle:@"Cancel"];
	[dialog addButtonWithTitle:@"OK"];
	//[listJoueurs replaceObjectAtIndex:button.tag-1 withObject:@"0"];
	nameField = [[UITextField alloc] initWithFrame:CGRectMake(12.0, 45.0, 260.0, 25.0)];
	nameField.tag = button.tag-1;
	nameField.text =@"";
	[nameField setBackgroundColor:[UIColor whiteColor]];
	[nameField setKeyboardType:UIKeyboardTypeNumberPad];
	
	[dialog addSubview:nameField];
	//CGAffineTransform moveUp = CGAffineTransformMakeTranslation(0.0, 100.0);
	//[dialog setTransform: moveUp];
	[dialog show];
	[dialog release];
	[nameField release];
	
}





/***** AD PLAYER **/
-(void) showAddPlayer:(int) num{
	*update = 1;
	UIAlertView* dialog = [[UIAlertView alloc] init];
	dialog.transform = CGAffineTransformMakeTranslation(0,60);
	
	[dialog setDelegate:self];
	[dialog setTitle:@"Enter chip count"];
	[dialog setMessage:@" "];
//[dialog addButtonWithTitle:@"Cancel"];
	[dialog addButtonWithTitle:@"OK"];
	[listJoueurs replaceObjectAtIndex:num withObject:@"0"];
	nameField = [[UITextField alloc] initWithFrame:CGRectMake(12.0, 45.0, 260.0, 25.0)];
	nameField.tag = num;
	nameField.text =@"";
	[nameField setBackgroundColor:[UIColor whiteColor]];
	//[nameField setKeyboardType: @"Number Pad"];
	[nameField setKeyboardType:UIKeyboardTypeNumberPad];
	
	[dialog addSubview:nameField];
	//CGAffineTransform moveUp = CGAffineTransformMakeTranslation(0.0, 100.0);
	//[dialog setTransform: moveUp];
	[dialog show];
	[dialog release];
	[nameField release];
	
	
	
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
//- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
	//NSLog(@"alert %@",[alertView textFieldAtIndex:0].text );
	[nameField resignFirstResponder]; // counter wait_fences: failed to receive reply: 10004003
	NSString *theString = [NSString stringWithFormat:@"%@", nameField.text];
	NSLog(@"alert %@ --- %d ",nameField.text,[theString length]);

	if ([theString length] > 0 )  {
		[listJoueurs replaceObjectAtIndex:nameField.tag withObject:theString];
	}
	[self.tableView reloadData];
}


/****/
/*
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {    //NSUInteger row = [indexPath row];
    tableSelection = indexPath;
    tapCount++;
	NSLog(@"TEST");
    switch (tapCount) {
        case 1: //single tap
            [self performSelector:@selector(singleTap) withObject: nil afterDelay: .4];
            break;
        case 2: //double tap
            [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(singleTap) object:nil];
            [self performSelector:@selector(doubleTap) withObject: nil];
            break;
        default:
            break;
    }
}

#pragma mark -
#pragma mark Table Tap/multiTap

- (void)singleTap {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Single tap detected" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];   
    tapCount = 0;
}

- (void)doubleTap {
    NSUInteger row = [tableSelection row];
   // companyName = [self.suppliers objectAtIndex:row]; 
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"DoubleTap" delegate:nil cancelButtonTitle:@"Yes" otherButtonTitles: nil];
    [alert show];
    tapCount = 0;
}*/
@end
