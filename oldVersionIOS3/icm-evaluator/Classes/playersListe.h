//
//  playersListe.h
//  icm-eval
//
//  Created by richard on 09/02/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface playersListe : UITableViewController{
	
	UITextField * nameField;
	NSMutableArray * listJoueurs;
	int nombreJoueurs;
	int tapCount;
	NSIndexPath *tableSelection;
	int *update;
}
- (IBAction) Edit:(id)sender;
@property(nonatomic,retain) NSMutableArray * listJoueurs;
@property int *update;
- (IBAction)AddButtonAction:(id)sender;
- (IBAction)DeleteButtonAction:(id)sender;

@end
