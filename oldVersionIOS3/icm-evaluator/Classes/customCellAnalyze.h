//
//  customCellAnalyze.h
//  icm-eval
//
//  Created by richard on 05/03/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface customCellAnalyze : UITableViewCell {

    UILabel *Titre;
	UILabel *pourcent;
	UILabel *equity;
	UILabel *poids;
	
	
}


@property (nonatomic, retain) UILabel *Titre;
@property (nonatomic, retain) UILabel *pourcent;
@property (nonatomic, retain) UILabel *equity;
@property (nonatomic, retain) UILabel * poids;
@end
