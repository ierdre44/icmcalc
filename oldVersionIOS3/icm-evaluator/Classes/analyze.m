//
//  analyze.m
//  icm-eval
//
//  Created by richard on 15/02/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "analyze.h"




@implementation analyze
@synthesize listIndice,listSection,listJoueurs,listPayouts,button;
@synthesize menuBas,update;
-(void)viewWillAppear:(BOOL)animated { 
//	[activityIndicator startAnimating];
	[menuBas addSubview:activityIndicator];
	[menuBas.superview  addSubview:activityView.view];
	//NSLog (@"---> %@",menuBas);
	//[activityIndicator release];
	timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(startCompute) userInfo:nil repeats:NO];
	
	//[self startCompute];
	//[activityIndicator stopAnimating];
	
	
}


- (void)viewDidLoad {
	
    [super viewDidLoad];
	activityView = [[ProgressView  alloc ] initWithNibName:@"ProgressView" bundle:nil];
	activityView.view.frame = CGRectMake(0, 0, 320	, 480);


	//activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];

	//self.listInfos = [NSMutableArray arrayWithObjects: @"first player",@"first player",nil];
	self.listIndice =  [NSMutableArray arrayWithObjects: @"1st",@"2nd",@"3rd",@"4th",@"5th",@"6th",@"7th",@"8th",@"9th",@"10th",nil];

	self.listSection = [NSMutableArray arrayWithObjects: @"",@"",@"",@"",@"",@"",@"",@"",@"",@"",@"",@"",nil];
	self.tableView.scrollEnabled = YES;
	

	[self.tableView setEditing:NO animated:NO];
//	self.tableView.userInteractionEnabled = YES;
//	[super setEditing:YES animated:YES]; 
	//[self.tableView setEditing:YES animated:YES];
	//[self.tableView setStyle: UITableViewStyleGrouped];
	[self.tableView initWithFrame:CGRectZero style:UITableViewStyleGrouped];	
	//UITableView *tableView = (UITableView *)[self.tableView viewWithTag:TableViewTag];
    self.tableView.backgroundColor = [UIColor clearColor];
	self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
	[self.tableView setSeparatorColor:[UIColor clearColor]];
	 [self.tableView setRowHeight:25];
	//self.tableView.backgroundColor = [UIColor blackColor];
	nombreJoueurs = 0;
	
	/*UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithTitle:@"Add" style:UIBarButtonItemStyleBordered target:self action:@selector(AddButtonAction:)];
	 [self.navigationItem setRightBarButtonItem:addButton];
	 */
	self.button = [UIButton buttonWithType:UIButtonTypeRoundedRect];

	self.tableView.editing = false;
	
	//tournoi.update = 0;


}


- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	
}

- (void)viewDidUnload {
	
}


- (void)dealloc {
    [super dealloc];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	NSLog(@"didSelectRowAtIndexPath");
	
	//[self.tableView reloadData];
	
	
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//	return tournoi.nbreChip;
    return [listJoueurs count];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	if (section ==0 ) return 4;
	//if (section == 1) return 4;
	
	return [listPayouts count] -1;
//	return [self.listInfos count];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
	
    
	/*stationCell 
	CustomCell *cell =   (CustomCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
	if (cell == nil) { 
		cell = [[[CustomCell alloc] 
				 initWithStyle:UITableViewCellStyleSubtitle 
				 reuseIdentifier:CellIdentifier] autorelease]; 
	} 
	cell.textLabel.text = [listInfos
						   objectAtIndex:indexPath.row]; 
	NSString *subtile = [NSString stringWithFormat:@"Player %d",indexPath.row+1];
	if ((indexPath.row ) < nombreJoueurs ) {
		cell.detailTextLabel.text = subtile ;
	}
	else 
		cell.detailTextLabel.text =@"";
	/*
	 UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	 [button addTarget:self
	 action:@selector(customActionPressed:)
	 forControlEvents:UIControlEventTouchDown];
	 [button setTitle:@"Custom Action" forState:UIControlStateNormal];
	 button.frame = CGRectMake(150.0f, 5.0f, 150.0f, 30.0f);
	 [cell addSubview:button];
	 */
	/*if (indexPath.section  == 0 ) {
		
	}
	
	cell.accessoryType = 
	UITableViewCellAccessoryDisclosureIndicator; 
	
	return cell; */
	static NSString *CellIdentifier = @"Cell";
	UITableViewCell *cell ;
	cell =   (CustomCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	//enlve le bleu sur selection des cellules 
	//cell.selectionStyle = UITableViewCellSelectionStyleNone;
	
	
	
	if (indexPath.section  ==0 ) {
		
		if (cell == nil) { 
		
			cell = [[[CustomCell alloc] 
					 initWithStyle:UITableViewCellStyleSubtitle 
					 reuseIdentifier:@"header"] autorelease];
			cell.selectionStyle = UITableViewCellSelectionStyleNone;
			if (indexPath.row == 0) {
				UIButton *buttonU = [UIButton buttonWithType:UIButtonTypeRoundedRect];
				[buttonU addTarget:self
							action:@selector(refreshCompute)
				 
				  forControlEvents:UIControlEventTouchDown];
				
				[buttonU setBackgroundImage:[[UIImage imageNamed:@"iphone_delete_button.png"]
											 stretchableImageWithLeftCapWidth:8.0f
											 topCapHeight:0.0f]
								   forState:UIControlStateNormal];
				
				[buttonU setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
				buttonU.titleLabel.font = [UIFont boldSystemFontOfSize:20];
				buttonU.titleLabel.shadowColor = [UIColor lightGrayColor];
				buttonU.titleLabel.shadowOffset = CGSizeMake(0, -1);
				//button.tag =indexPath.row ;
				[buttonU setTitle:@"refresh" forState:UIControlStateNormal];
				buttonU.frame = CGRectMake(200.0f, 5.0f, 80.0f, 30.0f);
				[cell addSubview:buttonU];
				
			}
			
		}
		
		
		switch (indexPath.row) {
		case 0:
//			cell.textLabel.text = @"Players: "; 
				
			cell.textLabel.text=[NSString stringWithFormat:@"Players: %d",tournoi.nbreJoueur];
			
					
			break;
		case 1: 	
			//cell.textLabel.text = @"ITM : "; 
			cell.textLabel.text=[NSString stringWithFormat:@"ITM: %d",[listPayouts count]-1];
			break;
		case 2: 	
			//cell.textLabel.text = @"Prizepool : "; 
			cell.textLabel.text=[NSString stringWithFormat:@"Prizepool: %d",tournoi.prizePool];
			break;
		case 3:	
			cell.textLabel.text=[NSString stringWithFormat:@"Chips: %d",tournoi.nbreChip];
			break;		
		default :
			break;
			
		}
		
		
	}
	//if ((indexPath.section  ==0 )&&  (indexPath.row ==0 )){
	/*if (false){	
		//UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
		[self.button addTarget:self
				   action:@selector(startCompute)
		 forControlEvents:UIControlEventTouchDown];
		[self.button setTitle:@"COMPUTE" forState:UIControlStateNormal];
		self.button.frame = CGRectMake(80, 0, 150.0f, 50.0f);
		
		[self.button setBackgroundColor:[UIColor whiteColor]];
		
		[self.button setTitleColor:[UIColor redColor] forState:UIControlStateNormal];

		[self.button setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
		
		[self.button setBackgroundImage:[UIImage imageNamed:@"normal.png"] forState:UIControlStateNormal];
		//[button setBackgroundImage:[UIImage imageNamed:@"disabled.png"] forState:UIControlStateDisabled];
		//[button setBackgroundImage:[UIImage imageNamed:@"selected.png"] forState:UIControlStateSelected];
		//[button setBackgroundImage:[UIImage imageNamed:@"higligted.png"] forState:UIControlStateHighlighted];
		//[button setBackgroundImage:[UIImage imageNamed:@"highlighted+selected.png"] forState:(UIControlStateHighlighted | UIControlStateSelected)];
		[cell addSubview:self.button];
		
		
		
		//cell.backgroundView = [[[UIView alloc] initWithFrame:CGRectZero] autorelease]; 
		
		
	}*/
	if (indexPath.section > 0) {
		if (cell == nil) { 
			customCellAnalyze *cellA = [[[customCellAnalyze alloc] 
					 initWithStyle:UITableViewCellStyleSubtitle 
					 reuseIdentifier:@"info"] autorelease];
			cellA.selectionStyle = UITableViewCellSelectionStyleNone;
			cellA.Titre.text = [listIndice objectAtIndex:indexPath.row];
			float val = tournoi.PlayerList[indexPath.section -1].proba[indexPath.row];
			if (val < 0) val =0;
			// Somme equity
		/*	int chP = 0;
			if (tournoi.nbreChip > 0){
			int ch = tournoi.PlayerList[indexPath.section -1].Chip;
			 chP= ch *100 / tournoi.nbreChip;
			}*/
			
			//
			
			
			cellA.pourcent.text = [NSString stringWithFormat:@"%.f %%",val];
			float val2 =(float)( val *  tournoi.gain[indexPath.row] )/ 100.0;
			cellA.equity.text = [NSString stringWithFormat:@"%.2f $",val2];
			//cellA.poids.text = [NSString stringWithFormat:@"%.2f $",chP];
			cell = cellA;
			
			
		} 
		
		
		//cell.textLabel.text=@"TESTE";
		
	}
	
	return cell;
}



/*- (IBAction)DeleteButtonAction:(id)sender{
 //[dataList removeLastObject];
 NSLog(@"DELL");
 
 
 }
 */
/*- (IBAction) Edit:(id)sender{
 NSLog(@"EDIT");
 
 }*/

- (UITableViewCellEditingStyle)tableView:(UITableView *)aTableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
	//if (indexPath.row == ([listInfos count]-1)) return UITableViewCellEditingStyleInsert;
	//return UITableViewCellEditingStyleDelete;
	return UITableViewCellEditingStyleNone;
	
}

/*-(IBAction)AddButtonAction:(id)sender{
 //[self.categories ;
 NSLog(@"ADD");
 
 
 //[categories  replaceObjectAtIndex:1 withObject:@"fsfsdfsd"];
 //[categories addObject:@"TEST"];
 //[self.tableView reloadData];
 }*/

- (void)tableView:(UITableView *)aTableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle 
forRowAtIndexPath:(NSIndexPath *)indexPath {
	if (editingStyle == UITableViewCellEditingStyleDelete) {
		//NSLog(@"D %d",nombreJoueurs);
		//[listInfos removeObjectAtIndex:indexPath.row];
		nombreJoueurs = nombreJoueurs -1;
		[self.tableView reloadData];
	} else if (editingStyle == UITableViewCellEditingStyleInsert) {
		//[self showAddPlayer:indexPath.row];
		//[listJoueurs replaceObjectAtIndex:indexPath.row withObject:res];
		//[listInfos insertObject:@"Add new player" atIndex:[listInfos count]];
		nombreJoueurs = nombreJoueurs +1;
		[self.tableView reloadData];
	}
}
- (IBAction) EditTable:(id)sender{
	if(self.editing)
	{
		[super setEditing:NO animated:NO];
		[self.tableView setEditing:NO animated:NO];
		[self.tableView reloadData];
		[self.navigationItem.leftBarButtonItem setTitle:@"Edit"];
		[self.navigationItem.leftBarButtonItem setStyle:UIBarButtonItemStylePlain];
	}
	else
	{
		[super setEditing:YES animated:YES];
		[self.tableView setEditing:YES animated:YES];
		[self.tableView reloadData];
		[self.navigationItem.leftBarButtonItem setTitle:@"Done"];
		[self.navigationItem.leftBarButtonItem setStyle:UIBarButtonItemStyleDone];
	}
}



- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
//- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
	//NSLog(@"alert %@",[alertView textFieldAtIndex:0].text );
	[nameField resignFirstResponder]; // counter wait_fences: failed to receive reply: 10004003
	NSString *theString = [NSString stringWithFormat:@"%@", nameField.text];
	NSLog(@"alert %@ --- %d ",nameField.text,[theString length]);
	
	if ([theString length] > 0 )  {
		//[listInfos replaceObjectAtIndex:nameField.tag withObject:theString];
	}
	[self.tableView reloadData];
}

#define SectionHeaderHeight 40


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
  /*  if ([self tableView:tableView titleForHeaderInSection:section] != nil) {
        return SectionHeaderHeight;
    }
    else {
        // If no section header title, no section header needed
        return 0;
    }*/
	if (section ==0 ) return 20;
	//if (section ==1 ) return 20;
	return SectionHeaderHeight;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    /*NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    if (sectionTitle == nil) {
        return nil;
    }
	*/
//	NSString *sectionTitle = @"SECTION";
	NSString *sectionTitle = [self.listSection objectAtIndex:section];
    // Create label with section title
    UILabel *label = [[[UILabel alloc] init] autorelease];
    label.frame = CGRectMake(20, 6, 300, 30);
    label.backgroundColor = [UIColor clearColor];
    /*label.textColor = [UIColor colorWithHue:(136.0/360.0)  // Slightly bluish green
                                 saturation:1.0
                                 brightness:0.60
                                      alpha:1.0];*/
	
    //label.shadowColor = [UIColor whiteColor];
    //label.shadowOffset = CGSizeMake(0.0, 1.0);
    label.font = [UIFont boldSystemFontOfSize:16];
    label.text = sectionTitle;
		
	
	
    // Create header view and add label as a subview
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, SectionHeaderHeight)];
	view.backgroundColor = [UIColor grayColor];
    [view autorelease];
    [view addSubview:label];
	
	if (section >0 ) {
		UILabel *eq = [[[UILabel alloc] init] autorelease];
//		eq.frame = CGRectMake(190, 6, 300, 30);
		eq.frame = CGRectMake(120, 6, 300, 30);

		eq.backgroundColor = [UIColor clearColor];
		eq.textColor = [UIColor orangeColor];
		
		
		eq.font = [UIFont boldSystemFontOfSize:16];
		NSString *theString = [NSString stringWithFormat:@"%.f$",  tournoi.PlayerList[section -1].equity];
		eq.text = theString;
		[view addSubview:eq];
		
		
		/*** Poids ***/ 
		int chP = 0;
		if (tournoi.nbreChip > 0){
			int ch = tournoi.PlayerList[section -1].Chip;
			chP= ch *100 / tournoi.nbreChip;
		}
		UILabel *ps = [[[UILabel alloc] init] autorelease];
		ps.frame = CGRectMake(120, 6, 300, 30);
		ps.backgroundColor = [UIColor clearColor];
		ps.textColor = [UIColor blackColor];
		
		
		ps.font = [UIFont boldSystemFontOfSize:16];
		NSString *theString2 = [NSString stringWithFormat:@"%d %%",  chP];
		ps.text = theString2;
		//[view addSubview:ps];
		if (tournoi.PlayerList[section -1].oldequity > 0 ) {
			UILabel *oe = [[[UILabel alloc] init] autorelease];
//			oe.frame = CGRectMake(250, 6, 300, 30);
			oe.frame = CGRectMake(190, 6, 300, 30);
			oe.backgroundColor = [UIColor clearColor];
//			oe.textColor = [UIColor blackColor];
			double value =  tournoi.PlayerList[section -1].equity-tournoi.PlayerList[section -1].oldequity ;
			
			oe.font = [UIFont boldSystemFontOfSize:16];
			if (value <0 ) {
				NSString *theString3 = [NSString stringWithFormat:@"%.1f$", value];
				oe.textColor = [UIColor redColor];
				oe.text = theString3;
			}
			else {
				double probC =0;
				if (tournoi.PlayerList[section -1].oldequity > 0) {
					probC = (tournoi.PlayerList[section -1].oldequity * 100 )/ (tournoi.PlayerList[section -1].equity );
				}
				//NSString *theString3 = [NSString stringWithFormat:@"+ %d (%.1f)$", value,probC];
				NSString *theString3;
				if ((probC > 0) && (value > 0)){
					theString3 = [NSString stringWithFormat:@"%.1f$(%.1f)", value,probC];
				}
				else {
					theString3 = [NSString stringWithFormat:@"%.1f$", value];

				}
				oe.textColor = [UIColor greenColor];
				oe.text = theString3;
				
			}
			[view addSubview:oe];
			//NSLog(@"********** %f %f",tournoi.PlayerList[section -1].oldequity,tournoi.update);
		}
		
		
	}
	
    return view;
}
-(void)refreshCompute{
	*update =1;
	//[NSTimer timerWithTimeInterval:0.1 target:self selector:@selector(startCompute:) userInfo:nil repeats:NO];
	[self startCompute];
}

-(void)startCompute{
	NSLog(@" ---- update %d",*update);
	if (*update == 0 ) {
		[activityView.view removeFromSuperview];
		return;
	}
	

	self.listSection = [NSMutableArray arrayWithObjects: @"",@"",@"",@"",@"",@"",@"",@"",@"",@"",@"",@"",nil];
	//[self.tableView  release];
	//self.tableView = [[UITableView alloc] init];
	
	init_sng(&tournoi);
	tournoi.update = tournoi.update +1;
	//Set player
	int prizePool=0;
	int chips=0;
	tournoi.nbreJoueur = [listJoueurs count] -1;
	if (tournoi.nbreJoueur  == 0 ) {
		[activityView.view removeFromSuperview];

		return;
	}
	if ([listPayouts count]  == 0 ) {
		[activityView.view removeFromSuperview];
		
		return;
	}
	
	for (int i =0 ; i< [listPayouts count]-1; i++ ){
		prizePool = prizePool +[[listPayouts objectAtIndex:i] intValue];
		tournoi.gain[i] = [[listPayouts objectAtIndex:i] intValue];
		
	}
	
	for (int i =0 ; i< [listJoueurs count]-1; i++) {
		chips = chips + [[listJoueurs objectAtIndex:i] intValue];
		tournoi.PlayerList[i].Chip =[ [listJoueurs objectAtIndex:i]intValue];
		NSString *subtile = [NSString stringWithFormat:@"Player %d",i+1];
		[self.listSection  replaceObjectAtIndex:i+1 withObject:subtile];
	}
	tournoi.nbreChip = chips;
	tournoi.prizePool = prizePool;
	
	
	
	solve_sng(&tournoi);
	//calcule max equity
	for (int i =0 ; i< [listJoueurs count]-1; i++) {
		tournoi.PlayerList[i].oldequity = tournoi.PlayerList[i].equity;
		//NSLog(@"OLD %f ",tournoi.PlayerList[i].oldequity);
		float  acc = 0;
		for (int j =0 ; j< [listPayouts count]-1; j++ ){

		int val = tournoi.PlayerList[i].proba[j];
		if (val < 0) val =0;
		
		
			acc   = tournoi.gain[j] * val  / 100.0 + acc ;
			
		
		}
		tournoi.PlayerList[i].equity = acc;
		//NSLog(@"NEW %f ",tournoi.PlayerList[i].equity);

//		NSLog (@" --> %f",acc); 
		
	}
	
	
	
//#define TEST 
/*	
#ifdef TEST
	NSLog(@"\n\n --> %d %d %d \n",tournoi.nbreChip,tournoi.nbreJoueur,tournoi.gain[0]);
	int j =0;
	int i=0;
	for (j=0; j < MAX_PLAYER ; j++ ){
		NSLog(@"--> Gain : %d %d \n",j+1,tournoi.gain[j]);
	}
	
	for (j=0; j < MAX_PLAYER ; j++ ){
		NSString *myString =@"";
		NSLog(@"| %d | \t ",tournoi.PlayerList[j].Chip);
		for (i=0; i < MAX_PLAYER ; i++ ) {
			NSString* result  =  [NSString stringWithFormat:@" %d ",tournoi.PlayerList[j].proba[i]];
			myString = [myString stringByAppendingString:result];
			
			//NSLog(@"\t%d ",tournoi.PlayerList[j].proba[i]);
		}
		NSLog(myString);
	}
#endif */
	
	//[self.tableView reloadData];
	//	NSTimer *
	timer=[NSTimer scheduledTimerWithTimeInterval:0.1 target:self.tableView  selector:@selector(reloadData) userInfo:nil repeats:NO];
	[timer isValid];
	//[activityIndicator stopAnimating];
	//[timer release];
	[activityView.view removeFromSuperview];
	*update = 0;
	
	
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	if ((indexPath.section ==0 ) && (indexPath.section ==0) )return 38;
	return 30;
}
@end