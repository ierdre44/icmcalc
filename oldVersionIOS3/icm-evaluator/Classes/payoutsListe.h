//
//  payoutsListe.h
//  icm-eval
//
//  Created by richard on 14/02/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface payoutsListe : UITableViewController{
	
	UITextField * nameField;
	NSMutableArray * listPayouts;
	NSMutableArray * listIndice;
	int payouts;
	int *update;
}
@property(nonatomic,retain) NSMutableArray * listPayouts;
@property(nonatomic,retain) NSMutableArray * listIndice;
@property int *update;
@end
