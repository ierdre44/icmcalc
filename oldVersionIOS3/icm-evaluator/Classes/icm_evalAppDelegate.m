//
//  icm_evalAppDelegate.m
//  icm-eval
//
//  Created by richard on 09/02/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//

#import "icm_evalAppDelegate.h"
#import "playersListe.h"

@implementation icm_evalAppDelegate

@synthesize window,Players,Players_tableau,menuBas,title,Analyze,Tab_Analyze;


- (void)applicationDidFinishLaunching:(UIApplication *)application {    

    // Override point for customization after application launch
    [window makeKeyAndVisible];
	
	
	// Wait TODO 
	//sleep(1); //SPLASH SCREEN DELAY
	//menu
	[menuBas setSelectedItem:[menuBas.items objectAtIndex:0]];	
	[menuBas setDelegate:self];
	/*	  UITabBarController * tabBarController = [[UITabBarController alloc] init];
		
	
	tabbarController.delegate = self;
	
	[window  addSubview:tabBarController.view];
		
	*/
	
	lP = [[playersListe alloc ] initWithStyle:UITableViewStylePlain];
	lPayout = [[payoutsListe alloc ] initWithStyle:UITableViewStylePlain];
	lAnalyze = [[analyze alloc ] initWithStyle:UITableViewStylePlain];
	lAnalyze.menuBas = [self menuBas];
	
	[Tab_Analyze addSubview:lP.view];
	[window addSubview:Players];	
	[self setPlayerTab:self];
	update = 0;


}
-(void) setPlayerTab:(id)sender{
	[current removeFromSuperview];
	title.title = @"Players";
	lP.view.frame = CGRectMake(0, 0, 320	,360);  
	lP.update =&update;
	[Players_tableau addSubview:lP.view];
	//[main addSubview:lP.view];
	//[window addSubview:Players];
 	current = lP.view;
	
}
-(void) setPayoutTab:(id)sender{
	title.title = @"Payouts";
	[current removeFromSuperview];
	
	lPayout.view.frame = CGRectMake(0, 0, 320	,360);  
	
	[Players_tableau addSubview:lPayout.view];
	lPayout.update = &update;
	current = lPayout.view;
	
}
-(void) setAnalyzeTab:(id)sender{
	[current removeFromSuperview];
	title.title = @"Analyze";
	
	lAnalyze.view.frame = CGRectMake(0, 0, 320	,360);  
	lAnalyze.listPayouts = lPayout.listPayouts;
	lAnalyze.listJoueurs = lP.listJoueurs;
	lAnalyze.update = &update;

	[Players_tableau addSubview:lAnalyze.view];
	
	current = lAnalyze.view;
	
	
	
}

- (void)dealloc {
    [window release];
    [super dealloc];
}


- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
	//NSLog(@"Did Select Here %d",menuBas.selectedItem == [menuBas.items objectAtIndex:0]);
	if (menuBas.selectedItem == [menuBas.items objectAtIndex:0]) {
		[self setPlayerTab:self];
	}
	else if (menuBas.selectedItem == [menuBas.items objectAtIndex:1]) {
	
		[self setPayoutTab:self];
	}
	else {
		[self setAnalyzeTab:self];	
	}
}


			

@end
