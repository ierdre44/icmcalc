//
//  ICM.m
//  S-G-wizard
//
//  Created by richard on 03/01/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//
//


//cc  ICM.m -o ICM -ObjC -framework Foundation




// Contrary to "Property List Programming Guide for Cocoa", reading from Mutable or Immutable container results in
// in a mutable container! This code will give you warnings, but will compile and run anyway.
// The way to inforce im/mutability is by using serialization API:
// books = [NSPropertyListSerialization propertyListFromData:binData
//mutabilityOption:NSPropertyListImmutable format:&format errorDescription:&error];
// This is a wrong way to do this, but it will still work!:
// NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:@"/Users/vpasman/test.plist"];       

// Below is the right way to do this:
/*
NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithContentsOfFile:@"/Users/vpasman/test.plist"];
NSArray *keys = [dict allKeys];
for (NSString *key in keys)
{
	NSLog(@"%@", key);
	NSObject *value = [dict objectForKey:key];
	if (value != nil)
	{
		NSString *class = [[value class] description];
		NSLog(@"%@", class);
		if ([class isEqualTo:@"NSCFNumber"])
		{
			NSNumber *num = (NSNumber *)value;
			NSLog(@"%@", num);
			if ([key isEqualToString:@"Birthdate"]) // we know it's a birthdate. this is for example purposes
			{
				[dict setObject:[NSNumber numberWithDouble:1564.0] forKey:key];
			}
		}
	}
}

NSDate *date = [NSDate date];
[dict setObject:date forKey:@"Date Read"];*/


/*******/
#import "ICM.h"


@implementation ICM

@end


//Combinaisons de k parmi n
/*int factorielle(int x) {
	if(x >= 1) {
		
		return (x*factorielle(x-1));
	}
	else  return 1;
}

int compter_combinaisons(int x,int y) {
	int z = y-x;
	return factorielle(y)/(factorielle(x)*factorielle(z));
}*/

NSArray * getTab(int p , int n ){

	//int r = compter_combinaisons(p,n);
	//printf("nbre comb %d ",r);
	NSString *file= [NSString stringWithFormat:@"file_%d_%d",p,n];
	NSString* plistPath = [[NSBundle mainBundle] pathForResource:file ofType:@"plist"];
	NSArray * contentArray = [NSArray arrayWithContentsOfFile:plistPath];
	
	NSLog(@"file_%d_%d ** ",p,n);
	//NSLog(@"** %@",contentArray );
	return contentArray;
}

float prod(int n, ...){
	va_list liste ;
	///int jeCompteLesArguments, resultat;
	int i,j;
	double tab[10];
	tab[0] = 0;
	//tab[0]=0;
	double res=0;
	va_start (liste, n);
		for (i = 0;
		 i < n;
		 i++) {
//		double s =va_arg (liste, double);
		tab[i] = va_arg (liste, double);
		printf("-- %d  * %f --" ,i,tab[i]);
	}
	double a = tab[0];
	res  = a/100.0;
	printf("+++ %.2f %f",res,tab[0]); 
	for (i =3; i< n; i++){
		double chip = tab[1];
		for (j =2; j < i; j++){
			chip = chip - tab[j];
		}
		res = res * (tab[i]*100 / chip);
		printf("+ %f %f\n" ,res,tab[0]);
	}
	
	return (res);
	
}
void solve_sng( sngStruct * sng){
	int i=0;
	int j =0;
	
	//Calcul  1er
	if (sng->gain[0]> 0){
	for (i=0 ; i < sng->nbreJoueur; i++){
		sng->PlayerList[i].proba[0] =((sng->PlayerList[i].Chip) *100 ) / sng->nbreChip ;
		
	}
	}
	
	//Calcul second 
	if (sng->gain[1]> 0){
		
		for (i=0 ; i < sng->nbreJoueur; i++){
			int sommeProba=0;
			for (j=0 ; j < sng->nbreJoueur; j++){
				if (i == j) continue ;
				sommeProba = sommeProba + (/**/(sng->PlayerList[j].proba[0])*(sng->PlayerList[i].Chip*100 /(sng->nbreChip -sng->PlayerList[j].Chip) ) /**/ );
			}
			sng->PlayerList[i].proba[1] = (sommeProba+50)/100;
		}
	}
	
	//calcul 3 
	//NSArray * pattern = getTab(2, sng->nbreJoueur)
	/*
	 Ici pour P joueur on va charger les combinaisons de joueurs des place pred
	 par exemple 
	 ici 3 place pour 5 joueur . Je dois explorer les combinaisons des deux premiers dans 4 joueur 
	 Pour calculer la proba du joueur 1 je dois étudier le cas où
	 2 finit 1 et 3 finit 2
	 et 
	 2 finit 1 et 4 finit 2
	 et 
	 2 finit 1 et 5 finit 2
	 et 
	 3 finit 1 et 4 finit 2
	 ....
	 soit 2 joueurs parmis P-1 (=k)  et ceux P fois 
	 pour joeur 2 je dois explorer 
	 1 finit 1 et 3 finit 2 
	 d'ou la structure du fichier
	 le getTab renvoie un tab de tab
	 les premiers record sont les listes de joueurs à prendre en comtpe en focntion de leur ordre
	 ex
	 R1 -> 2 3 4 5
	 R2 -> 1 3 4 5 
	 ...
	 puis les combinaisons 
	 C1  ->  1 2 
		-> 1 3 
		-> 1 4
 		-> 2 3
 		-> 2 4
 		-> 3 4
	 
	Et pour chaque R on calcul la bonne formule avec les C et on range le résultat dans proba[n]
	 dans notre exemple 2 pour la position 3
	 */
	/*if (sng->gain[2]> 0){
		//NSLog(@"** %@",getTab(2,sng->nbreJoueur));
		NSArray * pattern= getTab(2,sng->nbreJoueur);
		int l,m,n;
		int p;
		long sommeProba2=0;
		//long sommeProba3=0;
		for (p=0; p < sng->nbreJoueur ; p ++ ) {
			NSArray * others = [pattern objectAtIndex:p];
			sommeProba2=0;
			for (l=sng->nbreJoueur; l< [pattern count]  ; l++ ) {
				// somme P
				NSArray *Ps =  [pattern objectAtIndex:l];
				int Pa =1 ;
				int Cheap = 0;
				int pos1=[[others objectAtIndex:[[Ps objectAtIndex:0]intValue]] intValue];
				int pos2= [[others objectAtIndex:[[Ps objectAtIndex:1]intValue]] intValue];
				int pos0= p;
				float P= (sng->PlayerList[pos1].proba[0]);
				float S1=( sng->PlayerList[pos2].Chip *100)/ (sng->nbreChip - sng->PlayerList[pos1].Chip);
				float S2= ( sng->PlayerList[pos0].Chip*100)/ (sng->nbreChip - sng->PlayerList[pos1].Chip - sng->PlayerList[pos2].Chip);
				sommeProba2 = sommeProba2 + (P * S1 * S2) ;
			}
			sng->PlayerList[p].proba[2]=(((sommeProba2 + 5000)/100 ) +50)/100;
		}
	}*/
	
	if (sng->gain[2]> 0){
		NSLog(@" TEST GAIN 3" );
		//NSArray * pattern= getTab(3,sng->nbreJoueur);
		NSArray * pattern= getTab(2,sng->nbreJoueur);
		NSLog(@" TEST GAIN 3 %@",pattern);
		double sommeProba=0;
		int p,l;
		for (p=0; p < sng->nbreJoueur ; p ++ ) {
			NSArray * others = [pattern objectAtIndex:p];
			sommeProba=0;
			for (l=sng->nbreJoueur; l< [pattern count]  ; l++ ) {
				//proba J1
				
				NSArray *Ps =  [pattern objectAtIndex:l];
				int pos1=[[others objectAtIndex:[[Ps objectAtIndex:0]intValue]] intValue];
				int pos2= [[others objectAtIndex:[[Ps objectAtIndex:1]intValue]] intValue];
				float P=(sng->PlayerList[pos1].proba[0]);
				float C1 = (sng->PlayerList[pos1].Chip);
				float C2 = (sng->PlayerList[pos2].Chip);
				float C0 = (sng->PlayerList[p].Chip);
				float C = (sng->nbreChip);
				//double res = prod(6,P,C,C1,C2,C3,C0);
				double res = prod(5,P,C,C1,C2,C0);
				sommeProba = sommeProba + res/100;
			}
			sng->PlayerList[p].proba[2]=sommeProba;	
			NSLog(@" TEST GAIN 3 %d %d",sng->PlayerList[p].proba[2],sng->nbreJoueur);
			
		}
	}
	
	/** Gain 4 **/
	if (sng->gain[3]> 0){
		NSArray * pattern= getTab(3,sng->nbreJoueur);
		//NSArray * pattern= getTab(2,sng->nbreJoueur);
		
		double sommeProba=0;
		int p,l;
		for (p=0; p < sng->nbreJoueur ; p ++ ) {
			NSArray * others = [pattern objectAtIndex:p];
			sommeProba=0;
			for (l=sng->nbreJoueur; l< [pattern count]  ; l++ ) {
				//proba J1
				
				NSArray *Ps =  [pattern objectAtIndex:l];
				int pos1=[[others objectAtIndex:[[Ps objectAtIndex:0]intValue]] intValue];
				int pos2= [[others objectAtIndex:[[Ps objectAtIndex:1]intValue]] intValue];
				int pos3= [[others objectAtIndex:[[Ps objectAtIndex:2]intValue]] intValue];
				float P=(sng->PlayerList[pos1].proba[0]);
				float C1 = (sng->PlayerList[pos1].Chip);
				float C2 = (sng->PlayerList[pos2].Chip);
				float C3 = (sng->PlayerList[pos3].Chip);
				float C0 = (sng->PlayerList[p].Chip);
				float C = (sng->nbreChip);
				double res = prod(6,P,C,C1,C2,C3,C0);
				//double res = prod(5,P,C,C1,C2,C0);
				sommeProba = sommeProba + res/100/100;
			}
			sng->PlayerList[p].proba[3]=sommeProba;	
			NSLog(@" TEST GAIN 4 " );
			
		}
	}
	
	//getTab(3,4);
	/** Gain file_4 **/
	if (sng->gain[4]> 0){
		NSArray * pattern= getTab(4,sng->nbreJoueur);
		double sommeProba=0;
		int p,l;
		for (p=0; p < sng->nbreJoueur ; p ++ ) {
			NSArray * others = [pattern objectAtIndex:p];
			sommeProba=0;
			for (l=sng->nbreJoueur; l< [pattern count]  ; l++ ) {
				//proba J1
				
				NSArray *Ps =  [pattern objectAtIndex:l];
				int pos1=[[others objectAtIndex:[[Ps objectAtIndex:0]intValue]] intValue];
				int pos2= [[others objectAtIndex:[[Ps objectAtIndex:1]intValue]] intValue];
				int pos3= [[others objectAtIndex:[[Ps objectAtIndex:2]intValue]] intValue];
				int pos4= [[others objectAtIndex:[[Ps objectAtIndex:3]intValue]] intValue];

				float P=(sng->PlayerList[pos1].proba[0]);
				float C1 = (sng->PlayerList[pos1].Chip);
				float C2 = (sng->PlayerList[pos2].Chip);
				float C3 = (sng->PlayerList[pos3].Chip);
				float C4 = (sng->PlayerList[pos4].Chip);

				float C0 = (sng->PlayerList[p].Chip);
				float C = (sng->nbreChip);
				double res = prod(7,P,C,C1,C2,C3,C4,C0);
				//double res = prod(5,P,C,C1,C2,C0);
				sommeProba = sommeProba + res/100/100/100;
			}
			sng->PlayerList[p].proba[4]=sommeProba;	
			
		}
	}
	
	/** Gain file_5 **/
	if (sng->gain[5]> 0){
		NSArray * pattern= getTab(5,sng->nbreJoueur);
		double sommeProba=0;
		int p,l;
		for (p=0; p < sng->nbreJoueur ; p ++ ) {
			NSArray * others = [pattern objectAtIndex:p];
			sommeProba=0;
			for (l=sng->nbreJoueur; l< [pattern count]  ; l++ ) {
				//proba J1
				
				NSArray *Ps =  [pattern objectAtIndex:l];
				int pos1=[[others objectAtIndex:[[Ps objectAtIndex:0]intValue]] intValue];
				int pos2= [[others objectAtIndex:[[Ps objectAtIndex:1]intValue]] intValue];
				int pos3= [[others objectAtIndex:[[Ps objectAtIndex:2]intValue]] intValue];
				int pos4= [[others objectAtIndex:[[Ps objectAtIndex:3]intValue]] intValue];
				int pos5= [[others objectAtIndex:[[Ps objectAtIndex:4]intValue]] intValue];
				
				float P=(sng->PlayerList[pos1].proba[0]);
				float C1 = (sng->PlayerList[pos1].Chip);
				float C2 = (sng->PlayerList[pos2].Chip);
				float C3 = (sng->PlayerList[pos3].Chip);
				float C4 = (sng->PlayerList[pos4].Chip);
				float C5 = (sng->PlayerList[pos5].Chip);

				float C0 = (sng->PlayerList[p].Chip);
				float C = (sng->nbreChip);
				double res = prod(8,P,C,C1,C2,C3,C4,C5,C0);
				//double res = prod(5,P,C,C1,C2,C0);
				sommeProba = sommeProba + res/100/100/100/100;
			}
			sng->PlayerList[p].proba[5]=sommeProba;	
			
		}
	}
	
	/** Gain file_6 **/
	if (sng->gain[6]> 0){
		NSArray * pattern= getTab(6,sng->nbreJoueur);
		double sommeProba=0;
		int p,l;
		for (p=0; p < sng->nbreJoueur ; p ++ ) {
			NSArray * others = [pattern objectAtIndex:p];
			sommeProba=0;
			for (l=sng->nbreJoueur; l< [pattern count]  ; l++ ) {
				//proba J1
				
				NSArray *Ps =  [pattern objectAtIndex:l];
				int pos1=[[others objectAtIndex:[[Ps objectAtIndex:0]intValue]] intValue];
				int pos2= [[others objectAtIndex:[[Ps objectAtIndex:1]intValue]] intValue];
				int pos3= [[others objectAtIndex:[[Ps objectAtIndex:2]intValue]] intValue];
				int pos4= [[others objectAtIndex:[[Ps objectAtIndex:3]intValue]] intValue];
				int pos5= [[others objectAtIndex:[[Ps objectAtIndex:4]intValue]] intValue];
				int pos6= [[others objectAtIndex:[[Ps objectAtIndex:5]intValue]] intValue];

				float P=(sng->PlayerList[pos1].proba[0]);
				float C1 = (sng->PlayerList[pos1].Chip);
				float C2 = (sng->PlayerList[pos2].Chip);
				float C3 = (sng->PlayerList[pos3].Chip);
				float C4 = (sng->PlayerList[pos4].Chip);
				float C5 = (sng->PlayerList[pos5].Chip);
				float C6 = (sng->PlayerList[pos6].Chip);

				float C0 = (sng->PlayerList[p].Chip);
				float C = (sng->nbreChip);
				double res = prod(9,P,C,C1,C2,C3,C4,C5,C6,C0);
				//double res = prod(5,P,C,C1,C2,C0);
				sommeProba = sommeProba + res/100/100/100/100/100;
			}
			sng->PlayerList[p].proba[6]=sommeProba;	
			
		}
	}
	
	
	
}


void init_sng  (sngStruct*  sng){
	int i = 0;
	int j =0;
	sng->nbreChip = -1;
	sng->nbreJoueur = -1;
	
	for (i=0 ; i < MAX_PLAYER ; i ++ ) {
		sng->gain[i] = 0;
		sng->PlayerList[i].Chip =-1;
		for (j=0 ; j < MAX_PLAYER ; j ++ ){
			sng->PlayerList[i].proba[j] = -1;
			/*if (sng->update ==0 ) {
				sng->PlayerList[i].oldequity = 0;}
			else {
				sng->PlayerList[i].oldequity = sng->PlayerList[i].equity;}*/
			//sng->PlayerList[i].oldequity = sng->PlayerList[i].equity;
			//sng->PlayerList[i].equity = -1;
			
			
		}
	}
	
}
#ifdef TESTPC
int main(){
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	
#else 
	void test(){
#endif

	printf("TEST");
	sngStruct test ;
	init_sng(&test);
	
	test.nbreJoueur = 5;
	test.nbreChip = 10000;
	
	
	test.PlayerList[0].Chip=3750;
	test.PlayerList[1].Chip=3350;
	test.PlayerList[2].Chip=1400;
	test.PlayerList[3].Chip=1000;
//	test.PlayerList[3].Chip=1000;
	test.PlayerList[4].Chip=500;
		
	test.gain[0] = 5;
	test.gain[1] = 3;
	test.gain[2] = 2;
	test.gain[3] = 1;
	
	//test.gain[3] = 
		test.gain[4] = test.gain[5] =test.gain[6] =test.gain[7] =test.gain[8] =test.gain[9] =0 ;
	
	
	solve_sng(&test);
	
	
	
	printf("\n\n --> %d %d  \n",test.nbreChip,test.nbreJoueur);
	int j =0;
	int i=0;
	for (j=0; j < MAX_PLAYER ; j++ ){
		printf("--> Gain : %d %d \n",j+1,test.gain[j]);
	}
	
	for (j=0; j < MAX_PLAYER ; j++ ){
		
		printf("| %d | \t ",test.PlayerList[j].Chip);
		for (i=0; i < MAX_PLAYER ; i++ ) {
			
		printf("\t%d ",test.PlayerList[j].proba[i]);
		}
		printf("\n");
	}
#ifdef TESTPC
	[pool release];
#endif
	return 0;
}

