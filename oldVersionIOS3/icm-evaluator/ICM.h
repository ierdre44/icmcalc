//
//  ICM.h
//  S-G-wizard
//
//  Created by richard on 03/01/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
//#define TESTPC 1
#define MAX_PLAYER 20


/*#ifdef DEBUG
#define NSLog(FORMAT, ...) fprintf(stderr,"%s\n", [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);
#else
#define NSLog(...)
#endif
*/
#ifdef TESTPC
#include <stdio.h>
#else 
//#define printf(LISTE,...) NSLog(@LISTE,##__VA_ARGS__)
#define printf(LISTE,...) /* printf(LISTE,...) */
#endif
typedef struct ICM_INFO_J{
	unsigned int Chip;
	unsigned int proba[MAX_PLAYER];
	float equity;
	float oldequity;
}player_line;

typedef struct ICM_T{
	unsigned int nbreChip;
	unsigned int nbreJoueur;
	unsigned int gain[MAX_PLAYER];
	player_line PlayerList[MAX_PLAYER];
	unsigned int prizePool;
	unsigned int payout;
	unsigned int update;
	
}sngStruct;

void init_sng( sngStruct * sng);
void test(void);
void solve_sng( sngStruct * sng);

#ifdef TESTPC
@interface ICM  {
#else
@interface ICM  : NSObject{
#endif


}

@end



